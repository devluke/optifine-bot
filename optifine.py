import aiohttp

POST_URL = 'https://www.reddit.com/r/Optifine/comments/bh7o49/optifine_114_progress/'

async def get_status():
    url = f'{POST_URL}.json'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as r:
            if r.status == 200:
                json = await r.json()
                status = json[0]['data']['children'][0]['data']['link_flair_text']
                return status
