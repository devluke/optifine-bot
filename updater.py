from pathlib import Path

import asyncio

import optifine
import util

update_path = Path('data/update.txt')
current_status = None

async def prepare():
    try:
        status = update_path.read_text().strip()
        global current_status
        current_status = status
    except FileNotFoundError:
        pass

async def update_task(client):
    await client.wait_until_ready()
    while not client.is_closed():
        global current_status
        status = await optifine.get_status()
        if current_status != status:
            current_status = status
            update_path.write_text(status)
            for g in client.guilds:
                channels = g.text_channels
                channel = channels[0]
                for c in channels:
                    if str(c) == 'optifine-status':
                        channel = c
                        break
                await util.send(channel, f'OptiFine 1.14 is now {status}')
        await asyncio.sleep(10)
