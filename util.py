import discord.errors

async def send(channel, message):
    try:
        await channel.send(message)
    except discord.errors.Forbidden:
        pass
