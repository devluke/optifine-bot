# OptiFine Bot

A simple Discord bot that notifies servers when progress on OptiFine 1.14 has been made

## Inviting

Follow [this link](https://discordapp.com/oauth2/authorize?client_id=579465289473851402&scope=bot&permissions=8) to invite OptiFine bot to your server

## Usage

This bot will automatically send update messages to your server. If the channel `#optifine-status` exists, it will send all automatic messages to that channel. Otherwise, it will send them to the first channel on the server.

You can also automatically check the status by running the `optifine?` command in any text channel.

## Running

To run this bot, you first need to set some things up. Follow the steps below, creating all files and running all commands in the `optifine-bot` directory:

1. Create a directory named `data`.

2. Create a file named `config.py` and paste the following into that file, replacing `YOUR-TOKEN-HERE` with your bot token:

```python
DISCORD_CONFIG = {
    'token': 'YOUR-TOKEN-HERE'
}
```

3. Create a virtual environment by running the following command:

```bash
python3.7 -m venv venv
```

4. Activate the virtual environment you created by running the following command:

```bash
source venv/bin/activate
```

5. Install the required Python libraries by running the following command:

```bash
pip install -r requirements.txt
```

6. Run the bot by running the following command:

```bash
python bot.py
```

After setting the bot up, you can run it by running the following command:

```bash
source venv/bin/activate && python bot.py
```