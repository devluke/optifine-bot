import asyncio
import discord

import config
import optifine
import updater
import util

client = discord.Client()

@client.event
async def on_ready():
    activity = discord.Activity(name='a Reddit post', type=discord.ActivityType.watching)
    await client.change_presence(activity=activity)

    await updater.prepare()

    print(f'Member of {len(client.guilds)} guilds:')
    guilds = {}
    for g in client.guilds:
        print(f'    {g} ({g.member_count})')
    print(f'Logged in as {client.user}')

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.upper() == 'OPTIFINE?':
        status = await optifine.get_status()
        await util.send(message.channel, f'OptiFine 1.14 is {status}')

client.loop.create_task(updater.update_task(client))
client.run(config.DISCORD_CONFIG['token'])
